Windows 7.1 SDK, with extra features in DirectShow's DLLEntry.cpp,
showing how to correctly use DShow with MFC, 
as well as calling into MFC correctly when it is used. 

Successfully built in MBCS mode in Visual Studio 2019 Preview on 21/04/2020

Build Notes:
	- I used the latest Windows 10 SDK settings in the project.
	- NOTE: I simply used the files in the BaseClasses directory.
	- NOTE: I did not add additional includes or library dependencies from the SDK itself.
	- Read the notes in dllentry.cpp in BaseClasses if you are using MFC. You have to change your entry point in your DirectShow filter in this case.

